package main

import (
	"html/template"
  //"log"
	"net/http"
	"time"

  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promauto"
  "github.com/prometheus/client_golang/prometheus/promhttp"
  "github.com/urfave/negroni"
)

type HeroPageData struct {
  HeroPageTitle string
}

var (
  HttpServerSecondsUp = promauto.NewCounter(prometheus.CounterOpts{
    Namespace: "http",
    Name: "hero_server_up",
    Help: "Uptime",
  })

  opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
    Name: "hero_processed_ops_total",
    Help: "The total number of processed events",
  })

  requestDurations = prometheus.NewHistogramVec(prometheus.HistogramOpts{
    Namespace: "http",
    Name: "hero_request_seconds",
    Help: "The latency of the HTTP requests.",
    Buckets: prometheus.DefBuckets,
  }, []string{"name", "path"})

)

func recordMetrics() {
  go func() {
    for {
      HttpServerSecondsUp.Inc()
      time.Sleep(2 * time.Second)
    }
  }()
}

func requestDuration(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
  var startedAt = time.Now()
  next(rw, r)
  requestDurations.WithLabelValues("hero_request_duration", r.URL.Path).Observe(time.Since(startedAt).Seconds())
}


func main() {
  mux := http.NewServeMux()
  tmpl := template.Must(template.ParseFiles("template.html"))

  mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    data := HeroPageData{
      HeroPageTitle: "It’s dangerous to go alone, take this!",
    }
    tmpl.Execute(w, data)
  })

  fs := http.FileServer(http.Dir("img/"))
  mux.Handle("/img/", http.StripPrefix("/img/", fs))

  mux.Handle("/metrics", promhttp.Handler())

  n := negroni.Classic() // Includes some default middlewares
  n.UseHandler(mux)

  recordMetrics()

  prometheus.Register(requestDurations)
  n.Use(negroni.HandlerFunc(requestDuration))

  http.ListenAndServe(":8080", n)
}
